from typing import Dict, Tuple, List, Union, Optional


##############################################################################
# Framework Description
##############################################################################

# param_list
# param_list refer to a list of tuples and/or strings, i.e. List[Union[Tuple[str, Optional[str]], str]].
# Each item in the param_list corresponds to the executable, an option, a flag, or an argument.
# Executables and arguments are stored as strings, e.g. '"path/to/file"'.
# Options are stored as tuple pairs on the form (key, value), e.g. ('--min-length', '42').
# Flags are stored as tuple pairs on the form (flag, None), e.g. ('--quiet, None).

##############################################################################
# Private Functions
##############################################################################

def _condition(param_type: str, param_val: str) -> bool:
    if param_type not in ['OPTION', 'MULTIOPTION', 'FLAG']:
        return False
    if param_type == 'FLAG':
        return param_val not in [None, '', 'false']
    else:
        return param_val not in [None, '']


def _command_parser(param_name: str, param_type: str, param_val: str) -> List[Union[Tuple[str, Optional[str]], str]]:
    if param_type == 'OPTION':
        return [(f'--{param_name}', f'"{param_val}"')]
    elif param_type == 'FLAG':
        return [(f'--{param_name}', None)]
    elif param_type == 'MULTIOPTION':
        vals = param_val.split(',')
        return [(f'--{param_name}', f'"{v}"') for v in vals]
    else:
        raise ValueError(f"Type expected to be one of 'OPTION', 'FLAG', or 'MULTIOPTION'. Got '{param_type}'.")


def _parse_param(param_name: str, param_type: str, param_val: str) -> List[Union[Tuple[str, Optional[str]], str]]:
    if _condition(param_type, param_val):
        return _command_parser(param_name, param_type, param_val)
    else:
        return []


##############################################################################
# Public Functions
##############################################################################

def parse_param_list(environ: Dict[str, str], prefix: str) -> List[Union[Tuple[str, Optional[str]], str]]:
    """ Parse the items in 'environ' to command options

    Note: Assumes the keys in 'environ' are on the form '<prefix>_<type>_<parameter name>'.
    If not, the key will be skipped.

    """
    cmd = []
    for env_name in environ.keys():
        split = env_name.split('_')
        if split[0] == prefix and len(split) > 2:
            param_type = split[1]
            param_name = '-'.join(split[2:]).lower()
            param_val = environ[env_name]
            cmd += _parse_param(param_name, param_type, param_val)
    return cmd


def parse_params(environ: Dict[str, str], prefix: str) -> str:
    param_list = parse_param_list(environ, prefix=prefix)
    return param_list_to_str(param_list)


def param_list_to_str(param_list: List[Union[Tuple[str, Optional[str]], str]], param_sep: str = ' ') -> str:
    # Convert all params to tuples
    param_list = [(param,) if isinstance(param, str) else param for param in param_list]
    # Take away none print values
    exclude_values = [None, '']
    param_list = [[item for item in param if item not in exclude_values] for param in param_list]
    return param_sep.join([' '.join(param) for param in param_list])
