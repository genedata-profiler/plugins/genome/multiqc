import os
import re
from itertools import groupby
from pathlib import Path


def find_newest_folders(metrics_folder):
    folders = filter(lambda x: Path(metrics_folder, x).is_dir(), os.listdir(metrics_folder))
    res = []
    for key, value in groupby(sorted(folders, key=_base_name), _base_name):
        folder_versions = list(value)
        folder_versions = sorted(folder_versions, key=_version, reverse=True)
        res.append(folder_versions[0])
    return res


def split_folder_name(folder_name):
    m = re.match(r'(.*) \[([0-9]+)]$', folder_name)
    if m is None:
        return folder_name, 0
    else:
        base_name = m.groups()[0]
        version = int(m.groups()[1])
    return base_name, version


def _base_name(folder_name):
    name, version = split_folder_name(folder_name)
    return name


def _version(folder_name):
    name, version = split_folder_name(folder_name)
    return version
