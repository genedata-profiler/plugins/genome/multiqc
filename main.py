import os
import shutil
from pathlib import Path
from typing import List

from multiqc_code.input_filter import find_newest_folders
from utils import utils as u
from utils.settings_parser import parse_params

##############################################################################
# Environment Variables
##############################################################################

env_vars = dict(os.environ)

generic_input_layer = os.getenv('input_generic_data_0')
generic_output_layer = os.getenv('output_generic_data_0')


##############################################################################
# Output Files
##############################################################################

multiqc_outdir = os.environ['MULTIQC_OPTION_OUTDIR']


##############################################################################
# Input Files
##############################################################################

file_list_file = 'multiqc_input_files.txt'

u.console_print('Determining Input Folders:')

multiqc_input_folders: List[str]

if generic_input_layer is not None:
    multiqc_input_folders = []
    for f in os.listdir(generic_input_layer):
        multiqc_input_folders.append(Path(generic_input_layer, f).as_posix())
    if len(multiqc_input_folders) == 0:
        raise Exception("No input folders found in generic input data layer. Please run "
                        "compatible activities upstream of this activity.")
else:
    metrics_folder = Path(multiqc_outdir).parent.as_posix()
    newest_metrics_folders = find_newest_folders(metrics_folder)
    print(newest_metrics_folders)
    multiqc_input_folders = [Path(metrics_folder, f).as_posix() for f in newest_metrics_folders]
    if len(multiqc_input_folders) == 1:  # the MultiQC output folder itself is already created by the workflow system
        raise Exception("No input folders found in parent folder of Output Folder selected in "
                        "Genedata Profiler's data lake.")


with open(file_list_file, 'wt') as file:
    for folder in multiqc_input_folders:
        file.write(folder + '\n')


##############################################################################
# Run Command
##############################################################################

u.console_print('Run MultiQC')

cmd_options = parse_params(env_vars, 'MULTIQC')

cmd: str = f'multiqc "{file_list_file}" {cmd_options}' \
      f' --file-list' \
      f' --no-megaqc-upload' \
      f' --cl_config "show_analysis_paths: False"'

u.run_cmd(cmd, outfile=None, error_file=None)

if generic_output_layer is not None:
    shutil.copytree(multiqc_outdir, Path(generic_output_layer, 'multiqc'), dirs_exist_ok=True)
